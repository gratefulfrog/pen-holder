$fn=100;

DXF_FileName = "PenHolder_1.dxf";
DXF_ConeLayer = "0";
DXF_BaseLayer = "base";
/*
rotate_extrude()
    import(DXF_FileName,layer=DXF_ConeLayer);
*/
translate([0,0,-4.5])
  rotate_extrude()
    import(DXF_FileName,layer=DXF_BaseLayer);
